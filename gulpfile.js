'use strict';

const gulp = require('gulp');
const mjmlEngine = require('mjml').default;
const mjml = require('gulp-mjml');
const browsersync = require('browser-sync').create();

// Clean assets
function clean() {
    return del(["./dist"]);
}
// mjmlHtml
function mjmlHtml(done){
    gulp.src('src/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: true}, {beautify: false}))
        .pipe(gulp.dest('./dist'));
    done();
}

// mjmlHtmlBeautify
function mjmlHtmlBeautify(done) {
    gulp.src('src/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: false}, {beautify: true}))
        .pipe(gulp.dest('./dist'));
    done();
}

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./dist/"
        },
        port: 3000
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Watch files
function watchFiles() {
    gulp.watch('./src/*.mjml', (build));
    gulp.watch('./dist/*.html', (browserSyncReload));
}

// define complex tasks
const build = gulp.series(mjmlHtml);
const buildBeautify = gulp.series(mjmlHtmlBeautify);
const watch = gulp.parallel(watchFiles, browserSync);
const dev = gulp.series(build, watch);


// export tasks
exports.build = build;
exports.buildBeautify = buildBeautify;
exports.watch = watch;
exports.default = dev;
